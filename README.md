# Geonames API Task

Repository link: https://gitlab.com/b605/geonames_api

Test script located in repository if necessary.

In this task was implemented HTTP server to deliver information about geographical objects using FastAPI framework.

The data was taken from the link: http://download.geonames.org/export/dump/RU.zip

And the data format was explained on: http://download.geonames.org/export/dump/readme.txt

## Methods implemented:
* GET **/cities**
* GET **/cities/{geonameid}**
* GET **/cities/comparing**
* GET **/cities/hints**

## GET /cities

### Description:

Returns list of geographical objects.

### Example:

/cities?page=2&limit=120

### Query arguments:

* **page**: int (*default = 1*) - argument sets page to be shown, depends on limit argument
* **limit**: int (*default = 10*) - argument sets limit to be show on page

### Response example:
~~~json
{
"result": [
        {
        "geonameid": 451749,
        "name": "Zhukovo",
        "asciiname": "Zhukovo",
        "alternatenames": null,
        "latitude": "57.26429",
        "longitude": "34.20956",
        "feature_class": "P",
        "feature_code": "PPL",
        "country_code": "RU",
        "cc2": null,
        "admin1_code": "77",
        "admin2_code": null,
        "admin3_code": null,
        "admin4_code": null,
        "population": "0",
        "elevation": null,
        "dem": "237",
        "timezone": "Europe/Moscow",
        "modification_date": "2011-07-09"
        },
        {
        "geonameid": 451750,
        "name": "Zhitovo",
        "asciiname": "Zhitovo",
        "alternatenames": null,
        "latitude": "57.29693",
        "longitude": "34.41848",
        "feature_class": "P",
        "feature_code": "PPL",
        "country_code": "RU",
        "cc2": null,
        "admin1_code": "77",
        "admin2_code": null,
        "admin3_code": null,
        "admin4_code": null,
        "population": "0",
        "elevation": null,
        "dem": "247",
        "timezone": "Europe/Moscow",
        "modification_date": "2011-07-09"
        }
    ]
}
~~~

## GET /cities/{geonameid}

### Description:

Returns full information about geographical object.

### Example:

/cities/451749

### Path arguments:
* **geonameid**: int - object id

Response example:
```json
    {
    "geonameid": 451749,
    "name": "Zhukovo",
    "asciiname": "Zhukovo",
    "alternatenames": null,
    "latitude": "57.26429",
    "longitude": "34.20956",
    "feature_class": "P",
    "feature_code": "PPL",
    "country_code": "RU",
    "cc2": null,
    "admin1_code": "77",
    "admin2_code": null,
    "admin3_code": null,
    "admin4_code": null,
    "population": "0",
    "elevation": null,
    "dem": "237",
    "timezone": "Europe/Moscow",
    "modification_date": "2011-07-09"
    }
```


## GET /cities/comparing

### Description:

Compares two objects by the timezone and the distance to North.

### Example:

/cities/comparing?first_name=Стукшино&second_name=Сосенка

### Query arguments:

* **first_name**: str- first name (can be written in Cyrillic or Latin)
* **second_name**: str - second name (can be written in Cyrillic or Latin)

### Response example:
```json
    {
    "northest": "Сосенка",
    "is_same_timezone": false
    }
```

## GET /cities/hints

### Description:

*Additional method*

Show object names that match uncompleted request.

### Example:

/cities/hints?request=Сос&limit=10

### Query arguments:
* **request**: str- part of geographical object name (can be written in Cyrillic or Latin)
* **limit**: int (default=10) - limit of hints to be found

### Response example:
```json
{
"hints": [
    "Sosenka",
    "Ozero Sosnishchi",
    "Pristan’ Sosnitsa",
    "Sosnovka",
    "Vostochnyy Sosyk",
    "Verkhnyaya Sosnovka",
    "Verkhnyaya Sosnovka",
    "Vadovo-Sosnovka",
    "Tikhaya Sosna",
    "Staryye Sosny",
    "Staroye Sosno"
    ]
}
```


# How to run Docker

1. RU.txt file must be in the app directory (same as main.py, requirements.txt and Dockerfile)

2. Build docker image:

```bash
docker build -t geonames_api:1.0 .
```

3.  Run docker container:
```bash
docker run -d -p 8000:8000 geonames_api:1.0
```

# How to run without Docker:

1. RU.txt file must be in the app directory (same as main.py, requirements.txt)

2. Create virtual enviroment:

```bash
python3 -m venv venv
```

3. Activate virtual enviroment (Linux)

```bash
source venv/bin/activate
```

4. Install requirements:
```bash
pip install -r requirements.txt
```

5. Run server (on 0.0.0.0:8000 by default):
```bash
python3 main.py
```

# How to run tests

1. RU.txt file must be in the app directory (same as main.py, requirements.txt)

2. Create virtual enviroment:

```bash
python3 -m venv venv
```

3. Activate virtual enviroment (Linux)

```bash
source venv/bin/activate
```

4. Install development requirements:
```bash
pip install -r dev_requirements.txt
```

5. Run test:
```bash
pytest
```
